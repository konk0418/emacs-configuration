# Emacs Configuration
Currently this repository just has my `.spacemacs` file, as well as a customized `gruber-darker` theme of mine.

This Configuration is designed to run in a terminal without issue, it runs as gui as well, but doesn't look great, If you want a better looking gui, just use the default `gruber-darker` theme.
